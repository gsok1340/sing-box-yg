#!/bin/bash
export LANG=en_US.UTF-8
case "$(uname -m)" in
	x86_64 | x64 | amd64 )
	cpu=amd64
	;;
	i386 | i686 )
        cpu=386
	;;
	armv8 | armv8l | arm64 | aarch64 )
        cpu=arm64
	;;
	armv7l )
        cpu=arm
	;;
        mips64le )
        cpu=mips64le
	;;
        mips64 )
        cpu=mips64
	;;
        mips )
        cpu=mips
	;;
        mipsle )
        cpu=mipsle
	;;
	* )
	echo "当前架构为$(uname -m)，暂不支持"
	exit
	;;
esac
rm -rf /etc/openclash/core/clash_meta
wget -q -O /etc/openclash/core/clash_meta https://gitlab.com/rwkgyg/sing-box-yg/-/raw/main/version/$cpu
chmod +x /etc/openclash/core/clash_meta
meta=$(/etc/openclash/core/clash_meta -v 2>/dev/null |awk -F ' ' '{print $3}' | head -1)
echo -e "当前Openclash的Clash-meta内核版本：$meta"
